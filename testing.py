from calculator import ArithmeticOperations
import unittest

class TestArithmetic(unittest.TestCase):

    def setUp(self):
        self.arithmetic = ArithmeticOperations()

    def testAdd(self):
        self.assertEqual( self.arithmetic.add(2, 2),4)

    def testSubtract(self):
        self.assertEqual(self.arithmetic.subtract(7, 2),5)

    def testDivide(self):
        self.assertEqual(self.arithmetic.divide(8, 2),4)

    def testMultiply(self):
        self.assertEqual(self.arithmetic.multiply(2, 2),4)

unittest.main()
